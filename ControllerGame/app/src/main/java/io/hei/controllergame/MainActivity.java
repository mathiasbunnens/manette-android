package io.hei.controllergame;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class MainActivity extends AppCompatActivity {
    Button button_up, button_down, button_left, button_right, btn_start;
    TextView txtOuPut,txtOuPut2;
    WebSocket webSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button_up = (Button) findViewById(R.id.arrow_up);
        button_left = (Button) findViewById(R.id.arrow_left);
        button_right = (Button) findViewById(R.id.arrow_right);
        btn_start = (Button) findViewById(R.id.start);

        txtOuPut = (TextView) findViewById(R.id.output);
        txtOuPut2 = (TextView) findViewById(R.id.output2);

        instantiateWs();

        button_left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    webSocket.send("left\n");
                }
                else{
                    if(event.getAction() == MotionEvent.ACTION_UP){
                        webSocket.send("left-down\n");                    }
                }
                return true;
            }
        });

        button_right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    webSocket.send("right\n");
                }
                else{
                    if(event.getAction() == MotionEvent.ACTION_UP){
                        webSocket.send("right-down\n");
                    }
                }
                return true;
            }
        });

        button_up.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    webSocket.send("up\n");
                }
                return true;
            }
        });

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webSocket.send("start\n");
            }
        });
    }

    private void instantiateWs() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("wss://jouhei.ratal.eu").build();
        SocketListener socketListener = new SocketListener();
        webSocket = client.newWebSocket(request, socketListener);
        client.dispatcher().executorService();
    }

    public class SocketListener extends WebSocketListener {
        private static final int NORMAL_CLOSURE_STATUS = 1000;

        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            webSocket.send("pad\n");
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            output(text);
        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {

        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null);
            output("Closing : " + code + " / " + reason);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            output("Error : " + t.getMessage());
        }
    }

    private void output(final String txt) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(txt.equals("WELCOME\n")){
                    txtOuPut.setText(txt);
                }
                else{
                    txtOuPut2.setText("identifiant manette : " + txt);
                }
            }
        });
    }
}

